package ui.lair.roadClassification;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import ui.lair.imaging.ImageProcessor;

import android.util.Log;


public class BackProject_Classifier extends RoadClassifier {
	private List<Mat> roadExamples = new ArrayList<Mat>();

	private MatOfFloat ranges = new MatOfFloat(0f, 256f,0f, 256f,0f, 256f);

	private int numInput;
	private double[] outputProbabilities;
	private int cols;
	private int rows;
	private int intAggregateWidth;
	private int intAggregateHeight;
	private List<Mat> imgs = new ArrayList<Mat>();
	private Scalar avg;
	private Mat subRegion;
	
	public BackProject_Classifier(  int sX, int sY, int eX, int eY,
									int imageWidth, int imageHeight, int colRegions, int rowRegions){
		
		super(ImageProcessor.MODEL_TYPE_BACKPROJECT, sX, sY, eX, eY, imageWidth, imageHeight, colRegions, rowRegions);
		
		numInput = colRegions*rowRegions;
		outputProbabilities = new double[numInput];
		cols = imageWidth;
		rows = imageHeight;
		intAggregateWidth = cols/colRegions;
		intAggregateHeight = rows/rowRegions;
		imgs = new ArrayList<Mat>();
		
	}

	// IModel implementations

	
	@Override
	public void updateModel(Mat cameraImage){
		if(isBuildingLibrary()){
			Mat roadRegion = new Mat();
			roadRegion = handleSubRegion(cameraImage);
			Mat hist = ImageProcessor.caclulateHistogram(ImageProcessor.RGB, roadRegion, null);
			

			
			Log.e(this.getClass().getSimpleName(), hist.channels()+"");
			//Log.e(this.getClass().getSimpleName(), hist.rows() + "");
			

			roadExamples.add(hist);
		}
	}
	
	@Override
	public int getNumberOfModels(){
		
		return roadExamples.size();
	}

	@Override
	public double calculateRoadProbability(Mat region) {
		
		return 0;
	}
	
	@Override
	public Mat getModelHistogram(int index1, int index2) {
		return roadExamples.get(index1);
	}
	
	@Override
	public void deleteModels(){
		roadExamples.clear();
	}
	
	@Override
	public Mat handleSubRegion( Mat input1) {
		return input1.submat(startY, endY, startX, endX);
	}
	
	public double[] calc(Mat m_Rgba, int numCols, int numRows){
		

		for(int i = 0; i < numInput; i++){
			outputProbabilities[i] = Double.MIN_VALUE;
		}
		

		int pos = 0;
		
		imgs.clear();
		imgs.add(m_Rgba);
		
		Mat backproject = new Mat();
		backproject.create(m_Rgba.size(), m_Rgba.depth()); //sets up the mat to store the backprojection
		

		
		//Calculate road values
		for(int i = 0; i < roadExamples.size(); i++){
			pos=0;
			

			Imgproc.calcBackProject(imgs, new MatOfInt(0,1,2), getModelHistogram(i,0), backproject, ranges, 100);
			
			for(int x = 0; x < (m_Rgba.cols()); x+=intAggregateWidth){	
				for(int y = 0; y < (m_Rgba.rows()) ; y+=intAggregateHeight){
					


					subRegion = backproject.submat(y, y+intAggregateHeight, x, x+intAggregateWidth); 
					avg = Core.mean(subRegion);

					
					if(avg.val[0] > outputProbabilities[pos]){
						outputProbabilities[pos] = avg.val[0];
					}
					
					pos++;
				}
			}
		}
		

		return outputProbabilities;
	

	}


}
