package ui.lair.imaging;

public interface IAge {
	
	public double getAge();
	
	public double getNumberOfSelections();
	
	public void incrementAge();
	
	public void incrementNumberOfSelections();
	
	public double getSelectionPercentage();
}
