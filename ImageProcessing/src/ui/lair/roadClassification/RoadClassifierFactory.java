package ui.lair.roadClassification;

public class RoadClassifierFactory {
	
	public enum Model {RGB, HSV, RGB_HSV, TEX_BW, TEX_RGB, EDGE, BACKPROJECTION};

	public RoadClassifierFactory(){
		
	}
	
	public static RoadClassifier getRoadClassifier( Model type, int sX, int sY, int eX, int eY,
			int imageWidth, int imageHeight, int colRegions, int rowRegions){

		if(type == Model.BACKPROJECTION){
			return new BackProject_Classifier(sX,sY,eX,eY,imageWidth,imageHeight,colRegions,rowRegions);
		}
		else if(type == Model.RGB){
			return new RGB_Classifier(sX,sY,eX,eY,imageWidth,imageHeight,colRegions,rowRegions);
		}
		else if(type == Model.HSV){
			return new HSV_Classifier(sX,sY,eX,eY,imageWidth,imageHeight,colRegions,rowRegions);
		}
		else if(type == Model.RGB_HSV){
			return new RGB_HSV_Classifier(sX,sY,eX,eY,imageWidth,imageHeight,colRegions,rowRegions);
		}
		else{
			return null;
		}
	}
}
