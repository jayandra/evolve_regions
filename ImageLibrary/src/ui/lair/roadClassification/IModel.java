package ui.lair.roadClassification;

import org.opencv.core.Mat;

public interface IModel {

	
	public void updateModel(Mat cameraImage);
	
	
	
	public int getNumberOfModels();
	
	public double calculateRoadProbability(Mat region);
	
	public Mat getModelHistogram(int index1, int index2);
	
	public void deleteModels();
	
	public Mat handleSubRegion(Mat input1);
	
	public double[] calc(Mat image, int cols, int rows);
	
}
