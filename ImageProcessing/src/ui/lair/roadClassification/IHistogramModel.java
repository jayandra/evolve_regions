package ui.lair.roadClassification;

import org.opencv.core.Mat;

import ui.lair.imaging.Model;


public interface IHistogramModel {
	
	public void addModel(Mat newHistogram);
	
	public void updateModelExmaple(int intBestRoadModelMatchIndex, int intBestColorModelMatchIndex, double dblBestMatchValue, Model newModel);
	
	public void addNewRoadModel(Model newModel);

}
