package cotsbots.robot.robot;
/**
 * ViewBase.java
 * ViewBase is the base object class used for OpenCV.  This is an abstract class, 
 * so another class has to extend it. Image is drawn to the screen intent this class,
 * along with the FPS
 * 
 * @author stinger
 * */



import org.opencv.highgui.VideoCapture;
import org.opencv.highgui.Highgui;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public abstract class ViewBase extends SurfaceView implements
		SurfaceHolder.Callback, Runnable {

	// Size of image width and height to capture
	// with the camera
	public static final int sf_CAMERA_WIDTH = 640;
	public static final int sf_CAMERA_HEIGHT = 480;
	// Frames per second meter
	

	// Object for accessing screen canvas,
	// locking the screen for our application
	protected SurfaceHolder m_Holder;
	// camera object
	private VideoCapture m_Camera;

	// Boolean to determine if activity is paused so we can
	// stop execution.
	protected boolean m_IsActivityPaused = false;

	/**
	 * Creating a bitmap of correct size for the camera image we capture. we are
	 * using the ARGB bitmap (aka 4 bytes per pixel)
	 */
	protected Bitmap mBmpCanvas = Bitmap.createBitmap(sf_CAMERA_WIDTH,
			sf_CAMERA_HEIGHT, Bitmap.Config.ARGB_8888);

	/**
	 * Constructor for initializing use of phone screen and setting up FPS meter
	 * 
	 * @param context
	 */
	public ViewBase(Context context) {
		super(context);
		m_Holder = getHolder();
		m_Holder.addCallback(this);

	}

	public void surfaceChanged(SurfaceHolder _holder, int format, int width,
			int height) {
		synchronized (this) {
			if (m_Camera != null && m_Camera.isOpened()) {
				m_Camera.set(Highgui.CV_CAP_PROP_FRAME_WIDTH, sf_CAMERA_WIDTH);
				m_Camera.set(Highgui.CV_CAP_PROP_FRAME_HEIGHT, sf_CAMERA_HEIGHT);
			}
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		m_Camera = new VideoCapture(Highgui.CV_CAP_ANDROID);
		if (m_Camera.isOpened()) {
			(new Thread(this)).start();
		} else {
			m_Camera.release();
			m_Camera = null;
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		if (m_Camera != null) {
			synchronized (this) {
				m_Camera.release();
				m_Camera = null;
			}
		}
	}

	// Abstract function that inheriting classes have to override
	protected abstract void processFrame(VideoCapture capture);

	public void run() {
	
		while (!m_IsActivityPaused) {
			synchronized (this) {
				if (m_Camera == null)
					break;

				if (!m_Camera.grab()) {
					break;
				}
				// process camera image taken
				processFrame(m_Camera);
			}

			
		}
	}
}
